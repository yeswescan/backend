package com.example;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import yws.backend.Main;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GroupResourceTest {

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer(Main.TEST_URI);
        // create the client
        Client c = ClientBuilder.newClient();

        target = c.target(Main.TEST_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    public void testGet() {
		JsonObject obj = target.path("group/1").request().get(JsonObject.class);
		assertEquals(1, obj.getInt("groupId"));
		assertEquals("Tim Privat", obj.getString("groupName"));
        assertEquals("Private", obj.getString("groupType"));
		assertEquals(1, obj.getInt("admin"));
		assertEquals(1, obj.getJsonArray("members").getInt(0));
    }

    @Test
    public void testCreateAndJoin() {
		JsonObject newGroup = Json.createObjectBuilder()
				.add("groupName", "WG Frankengasse 25")
				.add("groupType", "Public")
				.add("admin", 2).build();
		JsonObject group = target.path("group/create").request().post(Entity.entity(newGroup, MediaType.APPLICATION_JSON_TYPE), JsonObject.class);
		int groupId = group.getInt("groupId");
		assertTrue(5 <= groupId);
		assertEquals("WG Frankengasse 25", group.getString("groupName"));
		assertEquals("Public", group.getString("groupType"));
		assertEquals(2, group.getInt("admin"));
		assertEquals(2, group.getJsonArray("members").getInt(0));
		JsonObject joinedGroup = target.path("group/" + groupId + "/join").queryParam("userId", 1).request().post(Entity.entity("", MediaType.APPLICATION_JSON_TYPE), JsonObject.class);
		assertEquals(groupId, group.getInt("groupId"));
		assertEquals(2, joinedGroup.getInt("admin"));
		assertEquals(1, joinedGroup.getJsonArray("members").getInt(0));
		assertEquals(2, joinedGroup.getJsonArray("members").getInt(1));
    }
}
