package yws.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by timethy on 10/11/14
 */
public enum DB {
	MIRACULIX;

	private Connection conn;

	private DB() {
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/miraculix?" +
					"user=dev&password=hackathon");
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	public Connection getConnection() {
		return conn;
	}
}
