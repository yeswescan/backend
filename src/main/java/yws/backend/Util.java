package yws.backend;

import javax.json.*;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.sql.Types.*;

/**
 * Created by timethy on 10/11/14
 */
public enum Util {
    UTIL;

    public JsonObjectBuilder rowToJsonObject(ResultSet r) throws SQLException {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        for (int i = 1; i <= r.getMetaData().getColumnCount(); i++) {
            String name = r.getMetaData().getColumnName(i);
            switch (r.getMetaData().getColumnType(i)) {
                case INTEGER:
                    obj.add(name, r.getInt(i));
                    break;
                case VARCHAR:
                    obj.add(name, r.getString(i));
                    break;
                case ARRAY:
                    obj.add(name, rowsToJsonArray(r.getArray(i).getResultSet()));
                    break;
                case DATE:
                    obj.add(name, r.getDate(i).getTime());
                    break;
                case BOOLEAN:
                case BIT:
                    obj.add(name, r.getBoolean(i));
                    break;
                case DOUBLE:
                    obj.add(name, r.getDouble(i));
                    break;
                case DECIMAL:
                    obj.add(name, r.getBigDecimal(i));
                    break;
                default:
                    System.out.println("Warning! not parsed: " + r.getMetaData().getColumnName(i) + ": " + r.getMetaData().getColumnTypeName(i));
                    break;
            }
        }
        return obj;
    }

    public JsonArray rowsToJsonArray(ResultSet r) throws SQLException {
        JsonArrayBuilder arr = Json.createArrayBuilder();
        while (r.next()) {
            arr.add(rowToJsonObject(r).build());
        }
        return arr.build();
    }

}
