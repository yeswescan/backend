package yws.backend;

import org.joda.time.DateTime;

import javax.json.*;
import java.util.Random;

/**
 * Created by timethy on 10/11/14
 */
public enum Mock {
	TEAM_WG;
	private final Random r = new Random();
	private Mock() {
		for(int i = 0; i < receipts.length; i++) {
			receipts[i] = mockReceipt();
		}
	}

	private int lastUserId = 0;
	private int lastGroupId = 0;
	private int lastBillId = 0;

	public final JsonObject teamGroup = Json.createObjectBuilder()
			.add("id", 1)
			.add("name", "Yes We Scan Team WG")
			.add("type", 0)
			.add("admin", 1)
			.add("users", Json.createArrayBuilder().add(1).add(2).add(3).add(4).build())
			.build();

	public final JsonObject[] users = new JsonObject[] {
			mockUser("Tim"), mockUser("Michael"), mockUser("David"), mockUser("Kevin")
	};
	public final JsonObject[] userGroups = new JsonObject[] {
			mockGroup(), mockGroup(), mockGroup(), mockGroup()
	};
	public final JsonObject[] receipts = new JsonObject[10];

	private JsonObject mockUser(String name) {
		lastUserId += 1;
		return Json.createObjectBuilder()
				.add("name", name)
				.add("password", "password")
				.add("email", name + "@yes-we-scan.ch")
				.add("id", lastUserId)
				.build();
	}

	private JsonObject mockGroup() {
		lastGroupId += 1;
		return Json.createObjectBuilder()
				.add("name", "Personal")
				.add("admin", lastGroupId)
				.add("members", Json.createArrayBuilder().add(lastGroupId))
				.add("id", lastGroupId)
				.build();
	}

	private JsonObject mockReceipt() {
		lastBillId += 1;
		int n = r.nextInt(50);
		JsonObjectBuilder receipt = Json.createObjectBuilder();
		receipt.add("createdate", new DateTime().getMillis());
		receipt.add("uploaddate", new DateTime().getMillis());
		receipt.add("location", "Migros HB");
		receipt.add("user", r.nextInt(4) + 1);
		receipt.add("photo", JsonValue.NULL);
		receipt.add("inputtype", "manual");
		JsonArrayBuilder items = Json.createArrayBuilder();
		for(int i = 1; i <= n; i++) {
			double price = r.nextInt(100)*1.0 + r.nextInt(100)*0.01;
			int count = r.nextInt(10);
			items.add(Json.createObjectBuilder()
					.add("nr", i)
					.add("bill", lastBillId)
					.add("article", "Another article")
					.add("price", price)
					.add("count", count)
					.add("total", price * count)
					.add("hidden", false)
					.add("group", r.nextInt(2) + 1)
					.build()
			);
		}
		receipt.add("items", items.build());
		return receipt.build();
	}
}
