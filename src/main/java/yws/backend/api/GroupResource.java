package yws.backend.api;


import yws.backend.DB;
import yws.backend.Util;

import javax.json.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.*;

@Path("group")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
/**
 * Created by timethy on 10/11/14
 */
public class GroupResource {
	@GET
	@Path("{id: \\d+}")
	public JsonStructure get(@PathParam("id") final int id) {
		try {
			PreparedStatement stmt = DB.MIRACULIX.getConnection().prepareStatement(
					"SELECT * FROM `Group` WHERE groupId = ?"
			);
			PreparedStatement getUserStmt = DB.MIRACULIX.getConnection().prepareStatement(
					"SELECT userId FROM InGroup WHERE groupId = ?"
			);
			stmt.setInt(1, id);
			ResultSet or = stmt.executeQuery();
			if(or.next()) {
				JsonObjectBuilder obj = Util.UTIL.rowToJsonObject(or);
				getUserStmt.setInt(1, id);
				JsonArrayBuilder users = Json.createArrayBuilder();
				ResultSet r = getUserStmt.executeQuery();
				while(r.next()) {
					users.add(r.getInt(1));
				}
				obj.add("members", users);
				return obj.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@POST
	@Path("create")
	public JsonStructure create(JsonObject group) {
		try {
			PreparedStatement stmt = DB.MIRACULIX.getConnection().prepareStatement(
					"INSERT INTO `Group` (`groupName`,`groupType`,`admin`)VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
			PreparedStatement inGroupStmt = DB.MIRACULIX.getConnection().prepareStatement(
					"INSERT INTO `InGroup` (`userId`,`groupId`)VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, group.getString("groupName"));
			stmt.setString(2, group.getString("groupType"));
			stmt.setInt(3, group.getInt("admin"));
			stmt.executeUpdate();
			ResultSet keys = stmt.getGeneratedKeys();
			if(keys.next()) {
				int key = keys.getInt(1);
				inGroupStmt.setInt(1, group.getInt("admin"));
				inGroupStmt.setInt(2, key);
				inGroupStmt.executeUpdate();
				return get(key);
			} else {
				return Json.createObjectBuilder()
						.add("error", "Error :-P")
						.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@POST
	@Path("{id: \\d+}/join")
	public JsonStructure join(@PathParam("id") final int id, @QueryParam("userId") final int userId) {
		try {
			PreparedStatement stmt = DB.MIRACULIX.getConnection().prepareStatement(
					"INSERT INTO `InGroup` (`groupId`, `userId`) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS
			);
			stmt.setInt(1, id);
			stmt.setInt(2, userId);
			stmt.executeUpdate();
			return get(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


    @POST
    @Path("{id: \\d+}/settlement")
    public JsonStructure settlement(JsonObject obj,@PathParam("id") final int id) {
        try {
            if (obj.containsKey("date") && obj.containsKey("user")) {
                Long date = obj.getJsonNumber("date").longValue();
                int user = obj.getInt("user");

                PreparedStatement insertSettlementStmt = DB.MIRACULIX.getConnection().prepareStatement(
                        "INSERT INTO `Settlement` (`groupId`,`settlementDate`,`settlementUser`) VALUES " +
                                "(?,?,?)", Statement.RETURN_GENERATED_KEYS);

                insertSettlementStmt.setInt(1,id);
                insertSettlementStmt.setDate(2, new java.sql.Date(date * 1000));
                insertSettlementStmt.setInt(3,user);

                ResultSet rsback = getBalance(id);
                ResultSet rsforward = getBalance(id);

                insertSettlementStmt.executeUpdate();
                ResultSet rs = insertSettlementStmt.getGeneratedKeys();
                rs.next();
                int settlementId = rs.getInt(1);

                double total = 0;
                double average = 0;
                int i = 0;
                while (rsback.next()){

                    total += rsback.getDouble("Total");
                    i+=1;
                }
                average = total / i;

                rsback.previous();
                rsforward.next();
                double high = rsforward.getDouble("Total") -average;
                double low = rsback.getDouble("Total") -average;
                boolean b = true;

                while(b) {
                    if (rsback.getInt("creator") != rsforward.getInt("creator")) {
                        if (high + low == 0) {
                            createTransaction(settlementId,rsback.getInt("creator"),rsforward.getInt("creator"),high);
                            rsforward.next();
                            rsback.previous();
                            high = rsforward.getDouble("Total") -average;
                            low = rsback.getDouble("Total") -average;

                        } else if (high + low < 0) {
                            createTransaction(settlementId,rsback.getInt("creator"),rsforward.getInt("creator"),high);
                            rsforward.next();
                            high = rsforward.getDouble("Total") -average;
                            low = high + low;

						} else{
                            createTransaction(settlementId,rsback.getInt("creator"),rsforward.getInt("creator"),low*(-1));
                            rsback.previous();
                            low = rsback.getDouble("Total") -average;
                            high = high + low;
                        }
                    } else {
                        b = false;
                    }
                }

				PreparedStatement getTransactionsStmt = DB.MIRACULIX.getConnection().prepareStatement(
						"Select transactionId,settlementId,fromUser,toUser,amount paid from Transaction where settlementId = ?");

				getTransactionsStmt.setInt(1, settlementId);

				return Util.UTIL.rowsToJsonArray(getTransactionsStmt.executeQuery());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GET
    @Path("{id: \\d+}/balance")
    public JsonStructure balance(@PathParam("id") final int id) {
        try {
            return Util.UTIL.rowsToJsonArray(getBalance(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ResultSet getBalance(int id){
        try {

            PreparedStatement getBalanceStmt = DB.MIRACULIX.getConnection().prepareStatement(
                   "Select * from ((Select R.creator, U.name , sum(quantity * itemPrice) as total From (Item I, Receipt R, User U) " +
                           "where R.receiptId = I.receiptId and R.creator = U.userId and R.uploadDate > (Select COALESCE(MAX(S.settlementDate),0) " +
                           "from Settlement S where S.groupId = I.groupId) and I.groupId = ? group by creator) UNION (SELECT U.userId, U.name ,0 From User U, " +
                           "InGroup IG where IG.userId = U.userId and IG.groupId = ?)) Q group by creator order by total desc");

            getBalanceStmt.setInt(1, id);
            getBalanceStmt.setInt(2, id);

            return getBalanceStmt.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void createTransaction(int settlementId, int fromUser, int toUser, double amount){
        try {

            PreparedStatement setTransactionStmt = DB.MIRACULIX.getConnection().prepareStatement(
                    "INSERT INTO `Transaction`(`settlementId`,`fromUser`,`toUser`,`amount`,`paid`,`transactionDate`)" +
                            " VALUES (?,?,?,?,0,NULL)");

            setTransactionStmt.setInt(1, settlementId);
            setTransactionStmt.setInt(2, fromUser);
            setTransactionStmt.setInt(3, toUser);
            setTransactionStmt.setDouble(4, amount);

            setTransactionStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
