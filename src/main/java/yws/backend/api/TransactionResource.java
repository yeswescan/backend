package yws.backend.api;

import yws.backend.DB;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonStructure;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by m on 11/10/14.
 */

@Path("transaction")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)


public class TransactionResource {

    public TransactionResource(){}

    @POST
    @Path("\"{id: \\d+/pay}")
    public JsonStructure create(JsonObject obj,@PathParam("id") final int id) {
        try {
                PreparedStatement insertSettlementStmt = DB.MIRACULIX.getConnection().prepareStatement(
                        "UPDATE `Transaction`SET `paid` = 1,`transactionDate` = ?" +
                                "WHERE `transactionId` = ?", Statement.RETURN_GENERATED_KEYS);

                insertSettlementStmt.setDate(1, new java.sql.Date(System.currentTimeMillis()));
                insertSettlementStmt.setInt(2, id);

                insertSettlementStmt.executeUpdate();

                return Json.createObjectBuilder()
                        .add("success", "all good ;)")
                        .build();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Json.createObjectBuilder()
                .add("error", "Invalid transaction")
                .build();

    }


}
