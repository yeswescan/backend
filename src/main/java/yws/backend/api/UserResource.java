package yws.backend.api;

import yws.backend.DB;
import yws.backend.Util;

import javax.json.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Path("user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
/**
 * Created by timethy on 10/11/14
 */
public class UserResource {
	public UserResource() {}

	@POST
	@Path("login")
	public JsonStructure login(JsonObject obj) {
		try {
			if(obj.containsKey("email") && obj.containsKey("password")) {
				String email = obj.getString("email");
				String password = obj.getString("password");
                //TODO Password case sensitve
				PreparedStatement getUserStmt = DB.MIRACULIX.getConnection().prepareStatement(
						"SELECT * FROM User WHERE email = ? AND password = ?"
				);
				getUserStmt.setString(1, email);
				getUserStmt.setString(2, password);
				JsonArray array = Util.UTIL.rowsToJsonArray(getUserStmt.executeQuery());
				if(!array.isEmpty()) {
					return array.getJsonObject(0);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Json.createObjectBuilder()
				.add("error", "Invalid credentials")
				.build();
	}


    @POST
    @Path("register")
    public JsonStructure register(JsonObject obj) {
        try {
            if(obj.containsKey("name") && obj.containsKey("email") && obj.containsKey("password")) {
                String name = obj.getString("name");
                String email= obj.getString("email");
                String password = obj.getString("password");
                PreparedStatement getUserStmt = DB.MIRACULIX.getConnection().prepareStatement(
                        "SELECT * FROM User WHERE email = ?");
                getUserStmt.setString(1, email);
                JsonArray array = Util.UTIL.rowsToJsonArray(getUserStmt.executeQuery());
				if(array.isEmpty()) {
					PreparedStatement insertUserStmt = DB.MIRACULIX.getConnection().prepareStatement(
							"INSERT INTO `User` (`name`,`email`,`password`) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);

                    insertUserStmt.setString(1, name);
                    insertUserStmt.setString(2, email);
                    insertUserStmt.setString(3, password);

                    insertUserStmt.executeUpdate();

                    return Util.UTIL.rowsToJsonArray(insertUserStmt.getGeneratedKeys());

                }else{
                    return Json.createObjectBuilder()
                            .add("error", "Email is already registred.")
                            .build();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Json.createObjectBuilder()
                .add("error", "Invalid credentials")
                .build();
    }

	/**
	 * Alle Gruppen, an denen der User beteiligt ist, zurückgeben.
	 * Gruppen mit { name, type, members: [uid-1, ..., uid-n], admin: uid-i }
	 */
	@GET
	@Path("{id: \\d+}/groups")
	public JsonArray getGroups(@PathParam("id") final int id) {
		try {
			PreparedStatement getUserStmt = DB.MIRACULIX.getConnection().prepareStatement(
					"SELECT g.* FROM `Group` AS g, InGroup AS i WHERE g.groupId = i.groupId AND userId = ?");
			getUserStmt.setInt(1, id);
			return Util.UTIL.rowsToJsonArray(getUserStmt.executeQuery());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@GET
	@Path("{id: \\d+}/open-transactions")
	public JsonStructure openTransactions(@PathParam("id") final int id) {
		try {
			PreparedStatement getTransactionsStmt = DB.MIRACULIX.getConnection().prepareStatement(
					"Select transactionId,settlementId,u1.name AS fromUser,u2.name AS toUser,amount paid " +
							"from Transaction, User u1, User u2 " +
							"where (fromUser = ? OR toUser = ?) AND u1.userId = fromUser AND u2.userId = toUser AND transactionDate IS NOT NULL");

			getTransactionsStmt.setInt(1, id);
			getTransactionsStmt.setInt(2, id);

			return Util.UTIL.rowsToJsonArray(getTransactionsStmt.executeQuery());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Alle Receipts, an denen der User direkt oder indirekt (über Gruppen) beteiligt ist, zurückgeben.
	 * Receipts mit {
	 *   createdate, uploaddate, location, user, pohto, inputtype,
	 *   groups: {gid-1: sum-1, ..., gid-n : sum-n},
	 *   items: [ siehe GET /item/:id ]
	 * }
	 */
	@GET
	@Path("{id: \\d+}/all-receipts")
	public JsonArray getReceipts(@PathParam("id") final int id) {
		try {
			PreparedStatement getUserReceiptsStmt = DB.MIRACULIX.getConnection().prepareStatement(
					"SELECT R.* FROM `Receipt` R, `Item` I, InGroup G " +
                            "WHERE R.receiptId = I.receiptId AND I.groupId = G.groupId " +
                            "AND G.userId = ? OR R.creator = ? " +
                            "Group by R.receiptId order by R.createDate");

            PreparedStatement getItemsStmt = DB.MIRACULIX.getConnection().prepareStatement(
                    "Select * from Item where receiptId = ? order by itemNr");
            PreparedStatement getGroupsStmt = DB.MIRACULIX.getConnection().prepareStatement(
                    "Select groupId, SUM(itemPrice*quantity) total from Item i where receiptId = ? GROUP BY groupId");

            getUserReceiptsStmt.setInt(1, id);
            getUserReceiptsStmt.setInt(2, id);

            ResultSet r = getUserReceiptsStmt.executeQuery();
            JsonArrayBuilder arr = Json.createArrayBuilder();

            while(r.next()) {
                getItemsStmt.setInt(1,r.getInt(1));
                getGroupsStmt.setInt(1,r.getInt(1));

                JsonObjectBuilder obj = Util.UTIL.rowToJsonObject(r);
                obj.add("items", Util.UTIL.rowsToJsonArray(getItemsStmt.executeQuery()));
                ResultSet g = getGroupsStmt.executeQuery();
                JsonObjectBuilder groups = Json.createObjectBuilder();
				double total = 0.0;
                while(g.next()) {
                    groups.add(Integer.toString(g.getInt(1)), g.getDouble(2));
					total += g.getDouble(2);
                }
				obj.add("total", total);
                obj.add("groups", groups.build());
                arr.add(obj);
            }

			return arr.build();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
