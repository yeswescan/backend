package yws.backend.api;

import yws.backend.DB;
import yws.backend.Util;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonStructure;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 * Created by m on 11/10/14.
 */
@Path("receipt")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)


public class ReceiptResource {

    public ReceiptResource(){}


    @POST
    @Path("create")
    public JsonStructure create(JsonObject obj) {
        try {
            if (obj.containsKey("store") && obj.containsKey("creator") &&
                    obj.containsKey("createDate") && obj.containsKey("mode") && obj.containsKey("image") ) {

                String store = obj.getString("store");
                int creator = obj.getInt("creator");

                long createDate = obj.getJsonNumber("createDate").longValue();

                String mode = obj.getString("mode");
                //String image = obj.getString("image");
                //TODO Insert Blob

                PreparedStatement insertReceiptStmt = DB.MIRACULIX.getConnection().prepareStatement(
                        "INSERT INTO `Receipt` (`store`,`creator`,`createDate`,`uploadDate`,`mode`,`image`)" +
                                " VALUES (?,?,?,?,?,NULL)", Statement.RETURN_GENERATED_KEYS);

                insertReceiptStmt.setString(1, store);
                insertReceiptStmt.setInt(2, creator);
                insertReceiptStmt.setDate(3, new java.sql.Date(createDate * 1000));
                insertReceiptStmt.setDate(4, new java.sql.Date(new Date().getTime()));
                insertReceiptStmt.setString(5, mode);

                insertReceiptStmt.executeUpdate();

                ResultSet rs = insertReceiptStmt.getGeneratedKeys();

                insertItems(rs.getInt(1), obj.getJsonArray("items"));

                return Util.UTIL.rowsToJsonArray(rs);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Json.createObjectBuilder()
                .add("error", "Invalid receipt")
                .build();
    }


    @POST
    @Path("\"{id: \\d+}")
    public JsonStructure edit(JsonObject obj, @PathParam("id") final int id) {
        try {
            if (obj.containsKey("store")) {

                String store = obj.getString("store");

                PreparedStatement insertReceiptStmt = DB.MIRACULIX.getConnection().prepareStatement(
                        "UPDATE `Receipt` SET `store` = ? WHERE `receiptId` = ?", Statement.RETURN_GENERATED_KEYS);

                insertReceiptStmt.setString(1, store);
                insertReceiptStmt.setInt(2, id);

                insertReceiptStmt.executeUpdate();

                ResultSet rs = insertReceiptStmt.getGeneratedKeys();

                PreparedStatement deleteItemsStmt = DB.MIRACULIX.getConnection().prepareStatement("DELETE FROM `Item` WHERE receiptId` = ?");
                deleteItemsStmt.setInt(1,id);
                deleteItemsStmt.executeUpdate();

                insertItems(rs.getInt(1), obj.getJsonArray("items"));

                return Util.UTIL.rowsToJsonArray(rs);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Json.createObjectBuilder()
                .add("error", "Invalid receipt")
                .build();
    }



    private void insertItems(int receiptId, JsonArray array) {
        try {
            PreparedStatement insertItemStmt = DB.MIRACULIX.getConnection().prepareStatement(
                    "INSERT INTO `Item` (receiptId,`itemNr`,`itemName`,`quantity`,`itemPrice`,`groupId`,`deleted`)" +
                            " VALUES (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);


            for (int i = 0; array.size() > i; i++) {
                JsonObject item = array.getJsonObject(i);

                if (item.containsKey("itemNr") && item.containsKey("itemName") && item.containsKey("quantity") &&
                        item.containsKey("itemPrice") && item.containsKey("groupId") && item.containsKey("deleted")) {

                    int itemNr = item.getInt("store");
                    String itemName = item.getString("itemName");
                    double quantity = item.getJsonNumber("quantity").doubleValue();
                    double itemPrice = item.getJsonNumber("itemPrice").doubleValue();
                    int groupId = item.getInt("groupId");
                    boolean deleted = item.getBoolean("deleted");

                    insertItemStmt.setInt(1, receiptId);
                    insertItemStmt.setInt(2, itemNr);
                    insertItemStmt.setString(3, itemName);
                    insertItemStmt.setDouble(4, quantity);
                    insertItemStmt.setDouble(5, itemPrice);
                    insertItemStmt.setInt(6, groupId);
                    insertItemStmt.setBoolean(7, deleted);

                    insertItemStmt.executeUpdate();
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
