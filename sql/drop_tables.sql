use miraculix;

drop table `Transaction`;
drop table `Settlement`;
drop table `Item`;
drop table `Receipt`;
drop table `InGroup`;
drop table `Group`;
drop table `User`;