use miraculix;

-- USER
INSERT INTO `miraculix`.`User` VALUES (1, "Timi", "t@timi.com", 1234);
INSERT INTO `miraculix`.`User` VALUES (2, "Kudi", "k@timi.com", 1234);
INSERT INTO `miraculix`.`User` VALUES (3, "Nina", "n@timi.com", 1234);
INSERT INTO `miraculix`.`User` VALUES (4, "Marco", "m@timi.com", 1234);

-- Group
INSERT INTO `miraculix`.`Group` VALUES (1,"Tim Privat","Private",1);
INSERT INTO `miraculix`.`Group` VALUES (2,"Kudi Privat","Private",2);
INSERT INTO `miraculix`.`Group` VALUES (3,"Nina Privat","Private",3);
INSERT INTO `miraculix`.`Group` VALUES (4,"Marco Privat","Private",4);
INSERT INTO `miraculix`.`Group` VALUES (5,"Tim WG","Public",1);
INSERT INTO `miraculix`.`Group` VALUES (6,"Kudi WG","Public",2);

-- InGroup
INSERT INTO `miraculix`.`InGroup` VALUES (1,1);
INSERT INTO `miraculix`.`InGroup` VALUES (2,2);
INSERT INTO `miraculix`.`InGroup` VALUES (3,3);
INSERT INTO `miraculix`.`InGroup` VALUES (4,4);
INSERT INTO `miraculix`.`InGroup` VALUES (1,5);
INSERT INTO `miraculix`.`InGroup` VALUES (3,5);
INSERT INTO `miraculix`.`InGroup` VALUES (4,5);
INSERT INTO `miraculix`.`InGroup` VALUES (1,6);
INSERT INTO `miraculix`.`InGroup` VALUES (2,6);
INSERT INTO `miraculix`.`InGroup` VALUES (3,6);
INSERT INTO `miraculix`.`InGroup` VALUES (4,6);

-- Receipt
INSERT INTO `miraculix`.`Receipt` VALUES (1,null,"Migros",1,'2014-01-01','2014-01-01',"M",null);
INSERT INTO `miraculix`.`Receipt` VALUES (2,null,"Migros",2,'2014-01-02','2014-01-02',"M",null);
INSERT INTO `miraculix`.`Receipt` VALUES (3,null,"Coop",3,'2014-01-03','2014-01-03',"M",null);
INSERT INTO `miraculix`.`Receipt` VALUES (4,null,"Migros",4,'2014-01-05','2014-01-05',"M",null);
INSERT INTO `miraculix`.`Receipt` VALUES (5,null,"Migros",1,'2014-01-06','2014-01-06',"M",null);
INSERT INTO `miraculix`.`Receipt` VALUES (6,null,"Migros",1,'2014-10-12','2014-10-12',"O",null);



-- Item
INSERT INTO `miraculix`.`Item` VALUES (6, 1, "Coca Cola 1.5", 1, 2.20, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 2, "MClass Sptzi", 1, 1.70, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 3, "Agnesi Arrabiate", 1, 2.80, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 4, "Zwiebeln", 1, 0.00, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 5, "Valflora Vollrahm UHT", 1, 1.70, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 6, "I am sop md & nf", 1, 3.60, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 7, "Poulet-Geschnetzeltes", 1, 11.30, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 8, "Coca Cola vanille", 1, 0.05, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (6, 9, "Wclass Chicken Sweet", 1, 4.90, 1, 0);


INSERT INTO `miraculix`.`Item` VALUES (1, 1, "A", 1, 20.05, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (1, 2, "B", 5, 2.55, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (1, 3, "C", 2, 3.30, 5, 0);
INSERT INTO `miraculix`.`Item` VALUES (1, 4, "D", 2, 0.05, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (1, 5, "E", 1, 2.0, 1, 0);

INSERT INTO `miraculix`.`Item` VALUES (2, 1, "A", 1, 20.05, 2, 0);
INSERT INTO `miraculix`.`Item` VALUES (2, 2, "B", 5, 2.55, 2, 0);
INSERT INTO `miraculix`.`Item` VALUES (2, 3, "C", 2, 3.30, 6, 0);
INSERT INTO `miraculix`.`Item` VALUES (2, 4, "D", 2, 0.05, 6, 0);
INSERT INTO `miraculix`.`Item` VALUES (2, 5, "E", 1, 2.0, 6, 0);

INSERT INTO `miraculix`.`Item` VALUES (3, 1, "A", 1, 20.05, 5, 0);
INSERT INTO `miraculix`.`Item` VALUES (3, 2, "B", 5, 2.55, 5, 0);
INSERT INTO `miraculix`.`Item` VALUES (3, 3, "C", 2, 3.30, 3, 0);
INSERT INTO `miraculix`.`Item` VALUES (3, 4, "D", 2, 0.05, 5, 0);
INSERT INTO `miraculix`.`Item` VALUES (3, 5, "E", 1, 2.0, 5, 0);

INSERT INTO `miraculix`.`Item` VALUES (4, 1, "A", 1, 20.05, 5, 0);
INSERT INTO `miraculix`.`Item` VALUES (4, 2, "B", 5, 2.55, 5, 0);
INSERT INTO `miraculix`.`Item` VALUES (4, 3, "C", 2, 3.30, 4, 0);
INSERT INTO `miraculix`.`Item` VALUES (4, 4, "D", 2, 0.05, 5, 0);
INSERT INTO `miraculix`.`Item` VALUES (4, 5, "E", 1, 2.0, 4, 0);

INSERT INTO `miraculix`.`Item` VALUES (5, 1, "A", 1, 20.05, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (5, 2, "B", 5, 2.55, 6, 0);
INSERT INTO `miraculix`.`Item` VALUES (5, 3, "C", 2, 3.30, 1, 0);
INSERT INTO `miraculix`.`Item` VALUES (5, 4, "D", 2, 0.05, 6, 0);
INSERT INTO `miraculix`.`Item` VALUES (5, 5, "E", 1, 2.0, 6, 0);


INSERT INTO `miraculix`.`Settlement` VALUES (1,5,'2014-01-03',1);

INSERT INTO `miraculix`.`Transaction` VALUES (1,1,2,1,2,0,'2014-01-03');
INSERT INTO `miraculix`.`Transaction` VALUES (2,1,3,1,4,0,'2014-01-03');
INSERT INTO `miraculix`.`Transaction` VALUES (3,1,4,1,5,0,'2014-01-03');