use miraculix;

CREATE TABLE `User`
(
	userId INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255),
	email VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	PRIMARY KEY (userId)
);
 

CREATE TABLE `Group`
(
	groupId INT NOT NULL AUTO_INCREMENT,
	groupName VARCHAR(255),
	groupType VARCHAR(50) NOT NULL, -- Private/Public
	admin INT,
	PRIMARY KEY (groupId),
	FOREIGN KEY (admin) REFERENCES `User`(userId) 
							on delete set NULL
							on update cascade
);

CREATE TABLE InGroup
(
	userId INT NOT NULL,
	groupId INT NOT NULL,
	PRIMARY KEY (userId,groupId),
	FOREIGN KEY (userId) REFERENCES `User`(userId) 
							on delete cascade
							on update cascade,
	FOREIGN KEY (groupId) REFERENCES `Group`(groupId) 
							on delete cascade
							on update cascade
);
	
CREATE TABLE Receipt
(
	receiptId INT NOT NULL AUTO_INCREMENT,
  sourceReceiptId INT,
	store VARCHAR(255),
	creator INT,
	createDate DATE,
	uploadDate DATE,
	`mode` VARCHAR(10) NOT NULL, -- (M(anual), O(CR), C(umlus)
	`image` LONGBLOB,
	PRIMARY KEY (receiptId),
	FOREIGN KEY (creator) REFERENCES `User`(userId) 
							on delete set null
							on update cascade
);

CREATE TABLE Item
(
	receiptId INT,
	itemNr INT NOT NULL,
	itemName VARCHAR(255),
	quantity DECIMAL(5,2),
	itemPrice DECIMAL(5,2),
	groupId Int,
	deleted BIT,
	PRIMARY KEY (receiptId,itemNr),
	FOREIGN KEY (receiptId) REFERENCES `Receipt`(receiptId) 
							on delete cascade
							on update cascade,
	FOREIGN KEY (groupId) REFERENCES `Group`(groupId)
							on delete set null
							on update cascade
);


CREATE TABLE Settlement
(
	settlementId INT NOT NULL AUTO_INCREMENT,
	groupId Int,
	settlementDate DATE,
	settlementUser INT,
	PRIMARY KEY (settlementId),
	FOREIGN KEY (groupId) REFERENCES `Group`(groupId)
							on delete cascade
							on update cascade,
	FOREIGN KEY (settlementUser) REFERENCES `User`(userId) 
							on delete set NULL
							on update cascade
);

CREATE TABLE `Transaction`
(
	transactionId INT NOT NULL AUTO_INCREMENT,
	settlementId Int,
	fromUser INT,
	toUser INT,
	amount DECIMAL(5,2),
	paid BIT,
	transactionDate DATE,
	PRIMARY KEY (transactionId),
	FOREIGN KEY (settlementId) REFERENCES `Settlement`(settlementId) 
							on delete cascade
							on update cascade,
	FOREIGN KEY (fromUser) REFERENCES `User`(userId) 
							on delete set NULL
							on update cascade,
	FOREIGN KEY (toUser) REFERENCES `User`(userId) 
							on delete set NULL
							on update cascade
);


